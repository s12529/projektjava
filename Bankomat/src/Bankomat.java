
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class Bankomat {
	
    private static Scanner odczyt;

	public static void main(String[] args) {
		final int EXIT = 0;
        Karta a = null;
       
        odczyt = new Scanner(System.in);
        List<Karta> karty = new ArrayList<Karta>();
        int wybor=1;
        double pieniadze=0;
        double stan=0;
        int pin;
 
        
        System.out.println("Utworz karte debetowa:\n1. Visa\n2. Master Card");
        try {
        wybor = odczyt.nextInt();	

        switch(wybor) 
        	{
            	case 1:
            		System.out.println("Prosze utworzyc PIN...");
            		pin = odczyt.nextInt();
            		System.out.println("Ile pieniedzy chcesz miec na koncie? (saldo poczatkowe)");
            		stan = odczyt.nextDouble();
            		a = new Visa(pin,stan);
            		break;
            case 2:
            	System.out.println("Prosze utworzyc PIN...");
                pin = odczyt.nextInt();
                System.out.println("Ile pieniedzy chcesz miec na koncie? (saldo poczatkowe)");
                stan = odczyt.nextDouble();
                 a = new MasterCard(pin,stan);
                break;
             default: System.out.println("Wybrano co� innego!");   
        	}
  	  } catch(InputMismatchException e) { System.err.println("Nieprawid�owy typ danych!"); }
        
         
        karty.add(a);
        
        while(wybor!=EXIT){
        try {
        System.out.println("0. Wyjscie\n1. Sprawdz PIN \n2. Sprawdz stan konta\n3. Wplata\n4. Wyplata\n5. Zakup Pre-paid");
        wybor = odczyt.nextInt();
        switch(wybor){
        	case 0: System.out.println("Do widzenia!");
            break;
            case 1:
                System.out.println("Twoj PIN to " + a.sprawdzPin());
                break;
            case 2:
                System.out.println(a.SprawdzStanKonta());
                break;
            case 3:
                System.out.println("Podaj kwote...");
                pieniadze = odczyt.nextDouble();
                a.wplata(pieniadze);
                break;
            case 4:
            	try {
               System.out.println("Podaj kwote...");
               pieniadze = odczyt.nextDouble();
                a.wyplata(pieniadze);
            	} catch(IllegalArgumentException e) {System.err.println("To nie jest karta kredytowa! Nie mozna miec ujemnego stanu konta...");}
                break;
            case 5:
                a.ZakupPrePaid();
                break;
        	}
        } catch( InputMismatchException e) { System.err.println("B��d bankomatu"); odczyt.next();}
          catch( NullPointerException e) { System.err.println("Brak karty w bankomacie!");}
        }

    }
        
}
